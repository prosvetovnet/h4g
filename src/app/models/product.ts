export interface Product {
  id: number,
  imgUrl: string,
  cost: number,
  name: string
}
