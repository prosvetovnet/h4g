import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatGridListModule, MatCardModule } from '@angular/material';
import { RoutingModule } from './app.routing'
import { AppComponent } from './app.component';
import { IdentificationService } from './services/identification.service';
import { CartService } from './services/cart.service';
import { OrdersService } from './services/orders.service';
import { HttpModule } from '@angular/http';

import {
  ProductsComponent,
  OrdersComponent,
  WelcomeComponent,
  HistoryComponent,
  ProductDetailComponent
} from './components';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    OrdersComponent,
    WelcomeComponent,
    ProductDetailComponent,
    HistoryComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    HttpModule,
    MatGridListModule,
    MatCardModule,
    RoutingModule
  ],
  providers: [IdentificationService, CartService, OrdersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
