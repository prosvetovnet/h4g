import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/throw';

@Injectable()
export class IdentificationService {

  private detectUrl = 'https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false'
  private identifyUrl = 'https://westeurope.api.cognitive.microsoft.com/face/v1.0/identify'

  constructor(private http: Http) { }

  public detectAndIdentify(buffer: ArrayBuffer): Observable<{ personId: string }> {
    return this
      .detect(buffer)
      .switchMap((someone: { faceId: string }) => this.identify(someone.faceId))
      .map((person: { personId: string }) => person)
  }

  public detect(buffer: ArrayBuffer): Observable<{ faceId: string }> {
    const headers = new Headers({
      'Content-Type': 'application/octet-stream',
      'Ocp-Apim-Subscription-Key' : 'eaead1c43d9e4a25ae02d67b74b5d103'
    })

    return Observable.create((obs: Observer<{}>) => {
      this.http
        .post(this.detectUrl, buffer, { headers })
        .map(res => res.json())
        .subscribe({
          next: (faces) => {
            if (faces.length) {
              obs.next(faces[0])
            }
          }
        })
    })
  }

  public identify(faceId: string): Observable<{}> {
    const params = {
      personGroupId: '4dhack',
      faceIds: [ faceId ],
      maxNumOfCandidatesReturned: 1,
      confidenceThreshold: 0.5
    }

    const headers = new Headers({
      'Content-Type': 'application/json',
      'Ocp-Apim-Subscription-Key' : 'eaead1c43d9e4a25ae02d67b74b5d103'
    })

    return Observable.create((obs: Observer<{ personId: string }>) => {
      this.http
        .post(this.identifyUrl, JSON.stringify(params), { headers })
        .map(res => res.json())
        .map(json => {
          console.log('Identified:', json[0].candidates[0])
          return json[0].candidates[0]
        })
        .subscribe({
          next: (person) => {
            if (person) {
              obs.next(person)
            } else {
              obs.next(null)
            }
          }
        })
    })
  }


  // start(blob: any): Observable<boolean> {



  //   return Observable.create((obs: Observer<boolean>) => {
  //     this.http
  //       .post(this.detectUrl, blob , { headers })
  //       .map(res => res.json())
  //       .subscribe({
  //         next: (data) => {
  //           if (data.length) {
  //             const faceId = data[0].faceId
  //             const params = {
  //               personGroupId: '4dhack',
  //               faceIds: [ faceId ],
  //               maxNumOfCandidatesReturned: 1,
  //               confidenceThreshold: 0.1
  //             }

  //             this.http
  //               .post(this.identifyUrl, JSON.stringify(params), { headers })
  //               .map(res => res.json())
  //               .subscribe({
  //                 next: (result) => {
  //                   console.log(result)
  //                   obs.next(true)
  //                 },
  //                 error: () => {
  //                   obs.next(false)
  //                 }
  //               })
  //           } else {
  //             obs.next(false)
  //           }
  //         },
  //         error: () => {
  //           obs.next(false)
  //         }
  //       })
  //   })
  // }

}
