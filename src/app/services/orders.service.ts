import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class OrdersService implements OnInit {

  private userId = ''

  orders_list;
  constructor() {}
  add(order) {
    if (!this.orders_list) { this.getLocalStorage(); }
    if (!this.orders_list) { this.orders_list = []}
    this.orders_list.unshift(order);
    if (this.userId) { localStorage.setItem(this.userId, JSON.stringify(this.orders_list));}
  }
  getLocalStorage() {
    this.orders_list = JSON.parse(localStorage.getItem(this.userId));
  }
  getOrders() {
    if (!this.orders_list) {this.getLocalStorage()}
    return this.orders_list;
  }
  ngOnInit() {
    this.getLocalStorage();
  }

  setUserId(value: string) {
    this.userId = value;
    this.getLocalStorage();
  }
}
