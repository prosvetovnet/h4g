import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../models'
import { CartService } from '../../services/cart.service'
import { OrdersService } from '../../services/orders.service'
import {window} from "rxjs/operator/window";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: [Product] = [
    {
      id: 0,
      name: 'Эспрессо',
      cost: 99,
      imgUrl: this.getProductUrl('espresso.jpg')
    },
    {
      id: 1,
      name: 'Каппучино',
      cost: 149,
      imgUrl: this.getProductUrl('cappuchino.jpg')
    },
    {
      id: 2,
      name: 'Американо',
      cost: 149,
      imgUrl: this.getProductUrl('americano.jpg')
    },
    {
      id: 3,
      name: 'Круассан',
      cost: 59,
      imgUrl: this.getProductUrl('cruissane.png')
    },
    {
      id: 4,
      name: 'Латте',
      cost: 129,
      imgUrl: this.getProductUrl('latte.jpg')
    },
    {
      id: 5,
      name: 'Mountain Dew',
      cost: 49,
      imgUrl: this.getProductUrl('mountain_dew.jpg')
    },
    /*{
      id: 6,
      name: 'Picnic',
      cost: 49,
      imgUrl: this.getProductUrl('picnic.jpg')
    },
    {
      id: 7,
      name: 'Twix',
      cost: 49,
      imgUrl: this.getProductUrl('twix.png')
    }*/
  ]
  products_in_cart = [];
  last_orders = [];

  private getProductUrl(name: string) {
    return `/assets/img/products/${name}`
  }

  constructor(private orders: OrdersService, private router: Router) { }

  addCard(priduct) {
    console.log(priduct)
    this.products_in_cart.push(priduct)
  }

  redOrder(order) {
    console.log('order', order)
    for (let i =0; i < order.length; i++) {
      this.addCard(order[i])
    }
  }

  delCard(index) {
    console.log(index);
    console.log(this.products_in_cart.splice(index, 1));
  }

  back() {
    location.reload();
  }

  by() {
    this.orders.add(this.products_in_cart);
    this.router.navigate(['orders'])
  }
  getLstOrders() {
    this.last_orders = this.orders.getOrders();
  }


  ngOnInit() {
    this.getLstOrders();
  }


}
