import { Component, OnInit } from '@angular/core';

import { OrdersService } from '../../services/orders.service'

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  last_orders = [];

  constructor(public OS: OrdersService) {

  }

  ngOnInit() {
    this.last_orders = this.OS.getOrders()
    console.log(this.last_orders);
  }

}
