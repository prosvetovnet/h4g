import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ElementRef } from '@angular/core';
import { Router } from '@angular/router'

import { IdentificationService, OrdersService } from '../../services'

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnDestroy {
  @ViewChild('video') videoEl: ElementRef;

  interval = null

  constructor(
    public IS: IdentificationService,
    private DS: DomSanitizer,
    private OS: OrdersService,
    private router: Router
  ) {
  }

  videoSrc: any;

  onVideoLoad() {
    this.interval = window.setInterval(() => {
      const canvas = document.createElement('canvas');

      const context = canvas.getContext('2d');
      context.drawImage(this.videoEl.nativeElement, 0, 0, canvas.width, canvas.height);

      canvas.toBlob(blob => {

        const reader = new FileReader()
        reader.onload = () => {

          const snapshot = reader.result
          this.IS
            .detectAndIdentify(snapshot)
            .subscribe((person) => {
              if (person !== null) {
                this.OS.setUserId(person.personId)
              }
              this.router.navigate(['products'])
            })

          // this.IS
          //   .detect(snapshot)
          //   .subscribe({
          //     next: (someone) => {

          //       this.IS
          //         .identify(someone.faceId)
          //         .subscribe({
          //           next: (person) => {
          //             if (person !== null) {
          //               console.log(person)
          //             }
          //           }
          //         })

          //     }
          //   })

        }

        reader.readAsArrayBuffer(blob)

      })
    }, 3 * 1000)
  }

  ngOnInit() {
    navigator.mediaDevices.getUserMedia({ video: true })
      .then((stream) => {
        this.videoSrc = stream
      })
  }

  ngOnDestroy() {
    window.clearInterval(this.interval)
  }
}
