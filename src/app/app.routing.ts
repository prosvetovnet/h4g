import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component'
import {
  OrdersComponent,
  ProductsComponent,
  WelcomeComponent,
  HistoryComponent,
  ProductDetailComponent,
} from './components'

const appRoutes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      },
      {
        path: 'products',
        component: ProductsComponent,
        pathMatch: 'full'
      },
      {
        path: 'products/:id',
        component: ProductDetailComponent,
      },
      {
        path: 'orders',
        component: OrdersComponent,
      },
      {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '*',
    redirectTo: ''
  }
]

export const RoutingModule = RouterModule.forRoot(appRoutes)
